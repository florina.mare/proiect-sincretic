# Problema Circuitului Calului pe Tabla de Șah

Acest proiect implementează rezolvarea problemei circuitului calului pe o tablă de șah de dimensiune variabilă.

## Descriere
Problema circuitului calului constă în găsirea unei călătorii complete a unui cal pe o tablă de șah, astfel încât calul să treacă printr-un singur pătrat o singură dată, urmând regulile de mișcare ale calului în șah.

## Cum Funcționează
Este Mutare Validă: Funcția este_mutare_valida verifică dacă o anumită mutare este validă pe tabla de șah, respectând limitele tablei și dacă poziția respectivă nu a fost deja vizitată.

Afișează Tabla: Funcția afiseaza_tabla este responsabilă pentru afișarea soluției pentru circuitul calului pe tabla de șah.

Rezolvare Tura Cavalerului: Funcția rezolva_tura_cavalerului inițializează tabla de șah și găsește soluția pentru problema circuitului calului, utilizând o abordare de tip backtracking.

Rezolvare Utilitară: Funcția rezolva_util este utilizată pentru a explora și a găsi soluția problemei circuitului calului, folosind backtracking pentru a testa mutările valide.

## Cum să Utilizezi Codul
Introducere Dimensiune Tablă: La rulare, programul va cere utilizatorului să introducă dimensiunea tablei de șah și coordonatele de start pentru cal.

Afișare Soluție: După introducerea datelor, programul va afișa soluția pentru circuitul calului pe tabla de șah sau un mesaj de eroare în cazul unei introduceri incorecte.

## Cum să Rulezi Codul
Clonează acest repository local.

Compilează codul utilizând un compilator C (de exemplu, gcc).

Rulează executabilul generat și urmează instrucțiunile pentru introducerea dimensiunii tablei și a coordonatelor de start.

## Observații
Asigură-te că dimensiunea tablei introdusă este corespunzătoare pentru a obține o soluție validă a problemei circuitului calului.

