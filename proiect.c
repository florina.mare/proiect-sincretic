#include <stdio.h>
#define N_MAX 10 // Poți ajusta dimensiunea maximă a tablei de șah conform cerințelor

// Prototipuri de funcții
int este_mutare_valida(int tabla[N_MAX][N_MAX], int N, int x, int y);
void afiseaza_tabla(int tabla[N_MAX][N_MAX], int N);
int rezolva_tura_cavalerului(int N, int start_x, int start_y);
int rezolva_util(int x, int y, int N, int tabla[N_MAX][N_MAX], int numar_mutari, int total_mutari);

int mutare_x[] = { 2, 1, -1, -2, -2, -1, 1, 2 };
int mutare_y[] = { 1, 2, 2, 1, -1, -2, -2, -1 };
int este_mutare_valida(int tabla[N_MAX][N_MAX], int N, int x, int y) {
    return (x >= 0 && x < N&& y >= 0 && y < N&& tabla[x][y] == -1);
}

void afiseaza_tabla(int tabla[N_MAX][N_MAX], int N) {
    printf("Solutia pentru circuitul calului:\n");
    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < N; ++j) {
            printf("%2d ", tabla[i][j]);
        }
        printf("\n");
    }
}

int rezolva_tura_cavalerului(int N, int start_x, int start_y) {
    int tabla[N_MAX][N_MAX];
      for (int i = 0; i < N; ++i) {
        for (int j = 0; j < N; ++j) {
            tabla[i][j] = -1;
        }
    }

   int numar_mutari = 0;
   tabla[start_x][start_y] = numar_mutari;
   int total_mutari = N * N;
   if (!rezolva_util(start_x, start_y, N, tabla, numar_mutari, total_mutari)) {
        printf("Nu există soluție\n");
        return 0;
    }

    afiseaza_tabla(tabla, N);
    return 1;
}

int rezolva_util(int x, int y, int N, int tabla[N_MAX][N_MAX], int numar_mutari, int total_mutari) {
    if (numar_mutari == total_mutari - 1) {
        return 1;
    }
for (int i = 0; i < 8; ++i) {
        int urmator_x = x + mutare_x[i];
        int urmator_y = y + mutare_y[i];

        if (este_mutare_valida(tabla, N, urmator_x, urmator_y)) {
            tabla[urmator_x][urmator_y] = numar_mutari + 1;
            if (rezolva_util(urmator_x, urmator_y, N, tabla, numar_mutari + 1, total_mutari)) {
                return 1;
            }
            tabla[urmator_x][urmator_y] = -1;
        }
    }

    return 0;
}

int main() {
    int N, start_x, start_y;

    printf("Introduceti dimensiunea tablei de sah (N): ");
    scanf("%d", &N);

    printf("Introduceti coordonatele de start (x y): ");
    scanf("%d %d", &start_x, &start_y);

    if (start_x < 0 || start_x >= N || start_y < 0 || start_y >= N) {
        printf("Coordonatele de start nu sunt valide pentru tabla de sah de dimensiune %d x %d.\n", N, N);
        return 1;
    }

    rezolva_tura_cavalerului(N, start_x, start_y);

    return 0;
}